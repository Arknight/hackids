class Proto {
    constructor() {
        this.position = 1;
        this.map = [" ", " ", " ", " ", " ", " ", " ", " ", " ", ">"];
        this.win = false
        this.moving = undefined
    }

    init() {
        this.showMap(this.position)
        console.log("\n" + "Proto no hace nada... Le la instrucción de la línea 18 y vuelve a intentar" + "\n")
    }

    jump() {
        setTimeout(() => {
                this.showMap(this.position)
                if (this.moving) {
                    clearInterval(this.moving)
                }
                console.log("\n" + "Proto salto, se golpeo con el techo y se rompio..." + "\n")
            }, 1000);
    }

    move() {
        this.changePosition()
        this.moving = setInterval(() => {
            this.changePosition()
            if (!this.win) {
                console.log("\n" + "Proto se mueve!!" + "\n")
            } else {
                clearInterval(this.moving)
                console.log("\n" + "Bien hecho recluta, Proto salio del deposito en una pieza, Bienvenido a Automaton."+ "\n")
            }
        }, 1000);
    }

    changePosition() {
        this.showMap(this.position)
        this.position += 1
        this.map[this.position - 2] = " "
        if (this.position > this.map.length) {
            this.win = true
        }
    }

    showMap(position) {
        console.clear();
        console.log(  "╔══════════╗" +
            "\n" + `║${this.showPosition(position)}║` +
            "\n" + "╚══════════╝");
    }

    showPosition(position) {
        let positionResult = "";
        this.map[position - 1] = "@"
        for (let i = 0; i < this.map.length; i++) {;
            let element = this.map[i];
            positionResult = `${positionResult}${element}`;
        }
        return positionResult;
    }
}

let proto = new Proto()

module.exports = {
    proto: proto
}
