Hola Recluta, así que quieres trabajar para nosotros. Pues ¡no tan rápido! aquí no aceptamos a cualquiera recluta, debes probar
que tienes las habilidades para manejar el... Automatón. El Automatón sera tu principal herramienta de trabajo, él hará lo que tú le digas
cuando tú se lo digas como tú se lo digas, si me demuestras que eres capas, tal vez tengas el potencial de hacer grandes cosas. Vamos a empezar
con "La prueba".

        La Prueba

Mira este mapa recluta:

╔══════════╗  
║@.........................>║  
╚══════════╝

¿Qué se ve simple dices? ¿Creíste que manejarías el verdadero Automatón en tu primer día? Piénsalo denuevo recluta, estas en la interfaz desde alli hablaras con el
Protomatón, una pieza de maquinaria sofisticada que solo comprende Javascriptiano. El Protomatón solo puede hacer un ápice de lo que hace el Automatón, pero solo eso
necesitas para pasar esta prueba. Ahora como ves en el mapa el protomatón está en un depósito estrecho, tienes que llevarlo a la salida en una pieza.

Aquí tienes la guía de Funciones del Protomatón!

Proto.move()

Proto.Jump()

¡¿Que no entiendes como usar estas funciones?! aquí tienes la protoguía, siempre estamos preparados para nuestros reclutas.

Las funciones son instrucciones específicas que los automatones(Clases) pueden ejecutar, estas fueron incertadas en ellos al momento de su creación
se componen de 3 partes:
                           1    2   3
                         Proto.move( )

La primera es el nombre del automatón(Clase) que hará dicha instrucción, es aquella que identifica quien o que la ejecuta.
La segunda parte es llamada método, y básicamente es la instrucción que el automatón realizara una vez ejecutada la orden. 
En tercera parte hay unos paréntesis allí se pueden agregar parámetros que cambian la forma en la que el automatón realizará la instrucción.

Siempre puedes ver la documentación principal aquí: Link a documentación de funciones de javascript