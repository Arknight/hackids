const start = require('./user_move.cjs')['start'];
const proto = require('./user_move.cjs')['proto'];

proto.init()

start().catch( err => {
    console.error("Proto no tiene esa funcion registrada en su sistema." + "\n" + "En las instrucciones estan declaradas las funciones utilizables por Proto" 
    + "\n" + "\n" + `Error: ${err.message}` + "\n")
})
